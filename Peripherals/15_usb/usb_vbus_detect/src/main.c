/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <hal/nrf_power.h>

int main(void)
{
	printk("Hello World! %s\n", CONFIG_BOARD);

	uint8_t vbus_detect_state = 0;

	while(1)
	{
		vbus_detect_state = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
		if(vbus_detect_state)
		{
			printk("USB cable connected\n");
		}
		else
		{
			printk("USB cable disconnected\n");
		}
		k_msleep(100);
	}

	return 0;
}
