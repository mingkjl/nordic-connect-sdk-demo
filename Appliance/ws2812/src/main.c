/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdio.h>
#include "ws2812.h"
#include "display_data.h"


int main(void)
{
	printf("Hello World! %s\n", CONFIG_BOARD);

	ws2812_drive_init();

	uint8_t rgb_data[FRAME_COL][FRAME_ROW][3] = {0};
	uint32_t row = 0, col = 0;
	uint32_t frame = 0;

	uint8_t rgbw_data[FRAME_COL][FRAME_ROW][4] = {0};
	uint8_t color = 255 / 4;

	uint8_t cnt = 0;
	uint8_t temp_data = 0;
	uint8_t data_dir = 0;
	uint8_t temp_data_a[4] = {0};


	while(1)
	{
		
		for(col = 0; col < FRAME_COL; col++)
		{
			for(row = 0; row < FRAME_ROW; row++)
			{                      
				rgb_data[col][row][0] = display_data[frame][col][row][1] / 20;
				rgb_data[col][row][1] = display_data[frame][col][row][0] / 20;
				rgb_data[col][row][2] = display_data[frame][col][row][2] / 20;

				// rgb_data[col][row][0] = 255 / 4;
				// rgb_data[col][row][1] = 255 / 4;
				// rgb_data[col][row][2] = 255	/ 4;
			}
		}

		if(frame < 108)
		{
			frame++;
		}
		else
		{
			frame = 0;
		}

		ws2812_frame_write(rgb_data);
		k_msleep(50);

	}
	return 0;
}
