# ifndef __LCD_INIT_H
# define __LCD_INIT_H

#include <hal/nrf_gpio.h>
#include <zephyr/kernel.h>  

#define ZERO_BIT 0x80
#define ONE_BIT 0xF0
#define RESET_BIT 150
#define FRAME_ROW 20
#define FRAME_COL 20


#define SPI_SCLK_PIN NRF_GPIO_PIN_MAP(0, 30)
#define SPI_MOSI_PIN NRF_GPIO_PIN_MAP(0, 28)


void ws2812_drive_init(void);
void ws2812_node_write(uint8_t rgb_data[3]);
void ws2812_reset(void);
void ws2812_frame_write(uint8_t rgb_data[FRAME_COL][FRAME_ROW][3]);

void rgbw_node_write(uint8_t rgb_data[4]);
void rgbw_frame_write(uint8_t rgb_data[FRAME_COL][FRAME_ROW][4]);

# endif