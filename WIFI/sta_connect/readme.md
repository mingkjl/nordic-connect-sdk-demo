## 简介
这个demo是使用nRF7002DK实现WIFI的STA模型连接到AP的例程，使用的SDK版本是V2.6.0;

## 关键代码
### 添加必要的Kconfig配置
```c
# Network
CONFIG_NETWORKING=y
CONFIG_WIFI=y
CONFIG_WIFI_NRF700X=y
CONFIG_WPA_SUPP=y

# Network Management
CONFIG_NET_MGMT=y
CONFIG_NET_MGMT_EVENT=y
CONFIG_NET_MGMT_EVENT_INFO=y
CONFIG_NET_MGMT_EVENT_STACK_SIZE=4096
CONFIG_NET_CONNECTION_MANAGER=y
CONFIG_WIFI_MGMT_EXT=y

# WIFI Credentials
CONFIG_WIFI_CREDENTIALS=y
CONFIG_WIFI_CREDENTIALS_STATIC=y
CONFIG_WIFI_CREDENTIALS_STATIC_SSID="<SSID>"
CONFIG_WIFI_CREDENTIALS_STATIC_PASSWORD="<PASSWORD>"

# Network Stack
CONFIG_NET_NATIVE=y
CONFIG_NET_SOCKETS=y
CONFIG_NET_L2_ETHERNET=y
CONFIG_NET_IPV6=n
CONFIG_NET_IPV4=y
CONFIG_NET_DHCPV4=y
```

`<SSID>`和`<PASSWORD>`是你的WIFI的SSID和密码，这里是静态配置的方式，连接固定的WIFI；

### 添加Memory的Kconfig配置
```c
# Memory
CONFIG_SYSTEM_WORKQUEUE_STACK_SIZE=4096
CONFIG_MAIN_STACK_SIZE=4096
CONFIG_HEAP_MEM_POOL_SIZE=153600
```

### 添加必要的头文件
在`main.c`中添加如下头文件

```c
#include <zephyr/logging/log.h>

#include <zephyr/net/wifi.h>
#include <zephyr/net/wifi_mgmt.h>
#include <zephyr/net/net_mgmt.h>
#include <net/wifi_mgmt_ext.h>
```

### 定义网络界面
```c
int main(void)
{
    // ...
    struct net_if *iface = net_if_get_first_wifi();
    if (iface == NULL) {
        LOG_ERR("Could not get iface");
        return -1;
    }

    // Wait for the network interface to be ready
    k_sleep(K_SECONDS(1));

    // ...
}
```
注意这里需要等待网络接口准备好，否则可能会出现连接失败的情况；

### 初始化和添加网络管理事件
在`main.c`中添加如下代码

```c
static struct net_mgmt_event_callback mgmt_cb;
static bool connected;

int main(void)
{
    // ...
    // initialize the mgmt callback
    net_mgmt_init_event_callback(&mgmt_cb, net_mgmt_event_handler, EVENT_MASK);
    net_mgmt_add_event_callback(&mgmt_cb);
    // ...
}
```

### 补充`net_mgmt_event_handler`函数实现

```c
static void net_mgmt_event_handler(struct net_mgmt_event_callback *cb,
            uint32_t mgmt_event, struct net_if *iface)
{
    if ((mgmt_event & EVENT_MASK) != mgmt_event) {
        return;
    }
    if (mgmt_event == NET_EVENT_L4_CONNECTED) {
        LOG_INF("Network connected");
        connected = true;
        return;
    }
    if (mgmt_event == NET_EVENT_L4_DISCONNECTED) {
        if (connected == false) {
            LOG_INF("Waiting for network to be connected");
        } else {
            LOG_INF("Network disconnected");
            connected = false;
        }
        return;
    }
}
```

### 配置WIFI连接参数
```c
int main(void)
{
    // ...
    // configure the wifi connection
	struct wifi_connect_req_params cnx_params;
	wifi_args_to_params(&cnx_params);
    // ...
}
```

### 补充`wifi_args_to_params`函数实现
```c
static int wifi_args_to_params(struct wifi_connect_req_params *params)
{
	params->ssid = CONFIG_WIFI_CREDENTIALS_STATIC_SSID;
	params->ssid_length = strlen(params->ssid);

	params->psk = CONFIG_WIFI_CREDENTIALS_STATIC_PASSWORD;
	params->psk_length = strlen(params->psk);

	params->channel = WIFI_CHANNEL_ANY;
	params->security = WIFI_SECURITY_TYPE_PSK;
	params->mfp = WIFI_MFP_OPTIONAL;
	params->timeout = SYS_FOREVER_MS;
	params->band = WIFI_FREQ_BAND_UNKNOWN;
	#if NCS_VERSION_NUMBER > 0x20600
	memset(params->bssid, 0, sizeof(params->bssid));
	#endif
	return 0;
}

```
这里`CONFIG_WIFI_CREDENTIALS_STATIC_SSID`和`CONFIG_WIFI_CREDENTIALS_STATIC_PASSWORD`是在Kconfig中配置的WIFI的SSID和密码；

### 发起WIFI连接请求
```c
int main(void)
{
    // ...
    // request the connection
	int err = net_mgmt(NET_REQUEST_WIFI_CONNECT, iface, &cnx_params, sizeof(struct wifi_connect_req_params));
	if (err) {
		LOG_ERR("Connecting to Wi-Fi failed, err: %d", err);
		return ENOEXEC;
	}

	k_sleep(K_SECONDS(10));
    // ...
}
```

### 编译和烧录程序
### 查看连接状态
编译烧录完成后，可以通过串口查看连接状态，会打印以下内容
```
Hello World! nrf7002dk_nrf5340_cpuapp
*** Booting nRF Connect SDK v3.5.99-ncs1 ***
[00:00:07.733,947] <inf> main: Network connected
```
我们可以看到`Network connected`表示连接成功；