#include <zephyr/kernel.h>
#include "nrfx_spim.h"
#include "hal/nrf_gpio.h"
#include <hal/nrf_spim.h>
#include "ws2812.h"

static nrfx_spim_t spi_instance = NRFX_SPIM_INSTANCE(3);


int spi_init(void)
{
    nrfx_spim_config_t spi_config = NRFX_SPIM_DEFAULT_CONFIG(SPI_SCLK_PIN, SPI_MOSI_PIN, NRF_SPIM_PIN_NOT_CONNECTED, NRF_SPIM_PIN_NOT_CONNECTED);
    spi_config.frequency = NRF_SPIM_FREQ_4M;

	int err_code = 0;

    err_code = nrfx_spim_init(&spi_instance, &spi_config, NULL, NULL);
	printk("SPI init err_code: %08x\n", err_code);
	return err_code;
}

void spi_write(uint8_t *data, uint16_t len)
{
    nrfx_spim_xfer_desc_t spi_tx_buff = NRFX_SPIM_XFER_TX(data, len);
    nrfx_spim_xfer(&spi_instance, &spi_tx_buff, 0);
}

void spi_read(uint8_t *data, uint16_t len)
{
    nrfx_spim_xfer_desc_t spi_rx_buff = NRFX_SPIM_XFER_RX(data, len);
    nrfx_spim_xfer(&spi_instance, &spi_rx_buff, 0);
}

void ws2812_node_write(uint8_t rgb_data[3])
{
    uint8_t data[24] = {0};
    uint8_t i = 0, j = 0;

    for(i = 0; i < 3; i++)
    {
        for(j = 0; j < 8; j++)
        {
            if(rgb_data[i] & (0x80 >> j))
            {
                data[i*8+j] = ONE_BIT;
            }
            else
            {
                data[i*8+j] = ZERO_BIT;
            }
        }
    }

    spi_write(data, 24);
}

void rgbw_node_write(uint8_t rgb_data[4])
{
    uint8_t data[36] = {0};
    uint8_t i = 0, j = 0;

    for(i = 0; i < 4; i++)
    {
        for(j = 0; j < 8; j++)
        {
            if(rgb_data[i] & (0x80 >> j))
            {
                data[i*8+j] = ONE_BIT;
            }
            else
            {
                data[i*8+j] = ZERO_BIT;
            }
        }
    }

    spi_write(data, 36);
}

void rgbw_frame_write(uint8_t rgb_data[FRAME_COL][FRAME_ROW][4])
{
    uint32_t row = 0, col = 0;

    for(col = 0; col < FRAME_COL; col++)
    {
        for(row = 0; row < FRAME_ROW; row++)
        {
            rgbw_node_write(rgb_data[col][row]);
        }
    }

    ws2812_reset();
}

void ws2812_frame_write(uint8_t rgb_data[FRAME_COL][FRAME_ROW][3])
{
    uint32_t row = 0, col = 0;

    for(col = 0; col < FRAME_COL; col++)
    {
        for(row = 0; row < FRAME_ROW; row++)
        {
            ws2812_node_write(rgb_data[col][row]);
        }
    }

    ws2812_reset();
}

void ws2812_reset(void)
{
    uint8_t data[RESET_BIT] = {0};
    spi_write(0x00, RESET_BIT);
}

void ws2812_drive_init(void)
{
    spi_init();
}

